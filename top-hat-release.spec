# top-hat-release.spec
#
# Release files based off of %{dist_name}-release.spec

# Naming
%global dist_name	top-hat 
%global dist_Name	Top Hat
%global _dist_Name	Top-Hat

# Versioning
%global dist_version 	0.0
%global code_name 	"The Dark Side of the Moon"

# Basing (on Fedora)
%global base_Name	Fedora
%global base_name	fedora
%global base_version	32


Summary:	Generic release files
Name:		top-hat-release
Version:	%{base_version}
Release:	1%{dist}
License:	GPL

URL:		https://gitlab.com/CheeseEBoi/top-hat-release
Source0:	%{URL}/-/archive/%{version}/%{name}-%{version}.tar.gz

BuildArch: noarch

Provides: %{dist_name}-release = %{version}-%{release}
Provides: %{dist_name}-release-variant = %{version}-%{release}

# We need to Provides: and Conflicts: system release here and in each
# of the top-hat-release-$VARIANT subpackages to ensure that only one
# may be installed on the system at a time.
Conflicts: system-release
Provides: system-release
Provides: system-release(%{base_version})
Conflicts:	fedora-release
Conflicts:	generic-release
Requires: %{dist_name}-release-common = %{version}-%{release}

%description
Generic release files such as yum configs and various /etc/ files that
define the release. This package explicitly is a replacement for the 
trademarked release package, if you are unable for any reason to abide by the 
trademark restrictions on that release package.


%package common
Summary: %{dist_Name} release files

Requires:   %{dist_name}-release-variant = %{version}-%{release}
Suggests:   %{dist_name}-release

Obsoletes:  redhat-release
Provides:   redhat-release
Obsoletes:  %{base_name}-release < 30-0.1

Obsoletes:  convert-to-edition < 30-0.7
Requires:   fedora-repos(%{base_version})

Conflicts: generic-release-common
Conflicts: fedora-release-common

%description common
%{dist_Name} release files common to all Desktop Environments


%package notes
Summary:	%{dist_Name} Release Notes
License:	Open Publication
Provides:	system-release-notes = %{version}-%{release}
Conflicts:	fedora-release-notes
Conflicts:	generic-release-notes

%description notes
%{dist_Name} release notes package. This package explicitly is a replacement
for the trademarked release-notes package, if you are unable for any reason
to abide by the trademark restrictions on that release-notes
package. Please note that there is no actual useful content here.


%prep
%setup

%build

%install
install -d %{buildroot}%{_prefix}/lib
echo "%{dist_Name} release %{dist_version} (%{code_name})" > %{buildroot}%{_prefix}/lib/fedora-release
echo "cpe:/o:generic:generic:%{base_version}" > %{buildroot}%{_prefix}/lib/system-release-cpe

# Symlink the -release files
install -d %{buildroot}%{_sysconfdir}
ln -s ../usr/lib/fedora-release %{buildroot}%{_sysconfdir}/fedora-release
ln -s ../usr/lib/system-release-cpe %{buildroot}%{_sysconfdir}/system-release-cpe
ln -s fedora-release %{buildroot}%{_sysconfdir}/redhat-release
ln -s fedora-release %{buildroot}%{_sysconfdir}/system-release

# Create the common os-release file
install -d $RPM_BUILD_ROOT/usr/lib/os.release.d/
cat << EOF >>%{buildroot}%{_prefix}/lib/os-release
NAME=%{dist_Name}
VERSION="%{dist_version} (%{code_name})"
ID=%{dist_name}
ID_LIKE=%{base_name}
VERSION_ID=%{base_version}
PRETTY_NAME="%{dist_Name} %{dist_version} (%{code_name})"
ANSI_COLOR="0;34"
LOGO=%{dist_name}-logo-icon
CPE_NAME="cpe:/o:generic:generic:%{base_version}"
HOME_URL=%{URL}
SUPPORT_URL="https://en.wikipedia.org/wiki/Help!_(album)"
BUG_REPORT_URL="%{URL}/-/issues"
REDHAT_BUGZILLA_PRODUCT="%{base_Name}"
REDHAT_BUGZILLA_PRODUCT_VERSION=%{base_version}
REDHAT_SUPPORT_PRODUCT="%{base_Name}"
REDHAT_SUPPORT_PRODUCT_VERSION=%{base_version}
PRIVACY_POLICY_URL="http://nsa.gov"
EOF

# Create the common /etc/issue
echo "\S" > %{buildroot}%{_prefix}/lib/issue
echo "Kernel \r on an \m (\l)" >> %{buildroot}%{_prefix}/lib/issue
echo >> %{buildroot}%{_prefix}/lib/issue
ln -s ../usr/lib/issue %{buildroot}%{_sysconfdir}/issue

# Create /etc/issue.net
echo "\S" > %{buildroot}%{_prefix}/lib/issue.net
echo "Kernel \r on an \m (\l)" >> %{buildroot}%{_prefix}/lib/issue.net
ln -s ../usr/lib/issue.net %{buildroot}%{_sysconfdir}/issue.net

# Create os-release and issue files for the different editions here
# There are no separate editions for %{dist_name}-release

# Create the symlink for /etc/os-release
ln -s ../usr/lib/os-release $RPM_BUILD_ROOT/etc/os-release

# Set up the dist tag macros
install -d -m 755 $RPM_BUILD_ROOT%{_rpmconfigdir}/macros.d
cat >> $RPM_BUILD_ROOT%{_rpmconfigdir}/macros.d/macros.dist << EOF
# dist macros.

%%fedora                %{base_version}
%%dist                %%{?distprefix}.fc%{base_version}%%{?with_bootstrap:~bootstrap}
%%fc%{base_version}                1
EOF

mkdir -p readme
install -pm 0644 README.%{_dist_Name}-Release-Notes readme

# Default system wide
for f in $(find presets/system)
do
	if ! [ -d $f ] 
	then
		install -Dm0644 $f -t $RPM_BUILD_ROOT%{_prefix}/lib/systemd/system-preset/
	fi
done

install -d $RPM_BUILD_ROOT%{_prefix}/lib/systemd/user-preset/
install -m0644 presets/90-default-user.preset $RPM_BUILD_ROOT%{_prefix}/lib/systemd/user-preset/


%files common
%{_prefix}/lib/fedora-release
%{_prefix}/lib/system-release-cpe
%{_sysconfdir}/os-release
%{_sysconfdir}/fedora-release
%{_sysconfdir}/redhat-release
%{_sysconfdir}/system-release
%{_sysconfdir}/system-release-cpe
%attr(0644,root,root) %{_prefix}/lib/issue
%config(noreplace) %{_sysconfdir}/issue
%attr(0644,root,root) %{_prefix}/lib/issue.net
%config(noreplace) %{_sysconfdir}/issue.net
%attr(0644,root,root) %{_rpmconfigdir}/macros.d/macros.dist
%dir %{_prefix}/lib/systemd/user-preset/
%{_prefix}/lib/systemd/user-preset/90-default-user.preset
%dir %{_prefix}/lib/systemd/system-preset/
%{_prefix}/lib/systemd/system-preset/85-display-manager.preset
%{_prefix}/lib/systemd/system-preset/90-default.preset
%{_prefix}/lib/systemd/system-preset/99-default-disable.preset


%files
%{_prefix}/lib/os-release


%files notes
%doc readme/README.%{_dist_Name}-Release-Notes


%changelog

