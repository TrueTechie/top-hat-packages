
Name: 		top-hat-installation
Version:	0.4
Release:	1%{?dist} 
Summary:	Anaconda configs for Top Hat
License:	GPL

# Sourcing
URL:		https://gitlab.com/CheeseEBoi/%{name}
Source0:	%{url}/-/archive/%{version}/%{name}-%{version}.tar.gz

BuildArch:	noarch

BuildRequires:	make

Requires:	anaconda
Requires:	initial-setup

%description
Configuration for Top Hat Linux's installer, Anaconda, based on the defaults of Fedora

%prep
%autosetup

%install
%{make_install} PREFIX=/usr

%files
/%{_sysconfdir}/anaconda/conf.d/02-top-hat.conf
/%{_sysconfdir}/initial-setup/conf.d/15-top-hat.conf
/%{_datadir}/anaconda/post-scripts/10-top-hat.ks

