# Naming
Name:		top-hat-gtk-config
Version:	0.1
Release:	1%{?dist}
Summary:	Settings for Top Hat with a GTK desktop
License:	GPL

# Sourcing
URL:		https://gitlab.com/CheeseEBoi/%{name}
Source0:	%{url}/-/archive/%{version}/%{name}-%{version}.tar.gz

Buildarch:	noarch

Requires:	qt5ct
Suggests:	vim-X11

%description
Configuration files for Top Hat Linux with a GTK based desktop.

%prep
%autosetup

%install
%{make_install} PREFIX=/usr

%files
/%{_sysconfdir}/skel/.profile
/%{_sysconfdir}/skel/.gvimrc

%changelog
* Sat Jul 18 2020 Elliot L <CheeseEBoi@mailo.com> - 0.1
- Initial version of package with qt5ct and gvim configuration files

